﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	
	private Paddle paddle;
	private bool hasStarted = false;
	private Vector3 paddleToBallVector;

	// Use this for initialization
	void Start () {
		paddle = GameObject.FindObjectOfType<Paddle> ();
		paddleToBallVector = this.transform.position - paddle.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		float mousePositionRelativeToBall = (Input.mousePosition.x / 50) - this.transform.position.x;
		if (!hasStarted) {

			// Lock the ball relative to the paddle
			this.transform.position = paddle.transform.position + paddleToBallVector;

			// Wait for a mouse click for launching the ball
			if (Input.GetMouseButton (0)) {
				print ("mouse clicked, launch ball");
				hasStarted = true;
				this.rigidbody2D.velocity = new Vector2 (mousePositionRelativeToBall + Random.Range (-2,2), 10f);
			}
		}
	}

	void OnCollisionEnter2D (Collision2D collision) {
		Vector2 tweak = new Vector2 (Random.Range (-0.3f, 0.3f), Random.Range (-0.3f, 0.3f));
		if (hasStarted) {
			audio.Play();
			this.rigidbody2D.velocity += tweak;
		}
	}
}
