﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

	public static MusicPlayer instance = null;

	// Runs before Start()
	void Awake () {
		if (instance == null) { // If this is the first instance,
			instance = this; // grab it to the static instance variable
			GameObject.DontDestroyOnLoad (gameObject); // and make it destroyable
			return; // and stop running this function
		}
		
		// At any other time, destroy the instance.
		Destroy (gameObject);
		Debug.Log ("Duplicate instance self-destructing");
	}
	
}
