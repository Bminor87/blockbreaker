﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {
	
	public Sprite[] hitSprite;
	public AudioClip crack;
	public static int breakableCount = 0;
	public GameObject smoke;

	private LevelManager levelManager;
	private int timesHit;
	private bool isBreakable;

	// Use this for initialization
	void Start () {
		isBreakable = (this.tag == "Breakable");
		// Keep track of breakable bricks
		if (isBreakable) {
			breakableCount++;
		}

		levelManager = GameObject.FindObjectOfType<LevelManager> ();
		timesHit = 0;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D(Collision2D collision) {
		AudioSource.PlayClipAtPoint (crack, transform.position);
		if (isBreakable) {
			HandleHits ();
		}
	}

	void HandleHits () {
		timesHit ++;
		int maxHits = hitSprite.Length + 1;
		if (timesHit >= maxHits) {
			breakableCount--;
			Debug.Log (breakableCount);
			Destroy (gameObject);
			PuffSmoke();
			levelManager.BrickDestroyed();
		} else {
			LoadSprites();
		}
	}

	void PuffSmoke () {
		GameObject smokeInstance = (GameObject) Instantiate (smoke, this.transform.position, Quaternion.identity);
		smokeInstance.particleSystem.startColor = this.GetComponent<SpriteRenderer>().color;
	}

	void LoadSprites () {
		int spriteIndex = timesHit - 1;

		if (hitSprite [spriteIndex] != null) {
			this.GetComponent<SpriteRenderer> ().sprite = hitSprite [spriteIndex];
		} else {
			Debug.LogError ("Sprite Missing");
		}
	}
}
